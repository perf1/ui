import { put, takeLatest, all, select } from "redux-saga/effects";
import * as actionTypes from "../constants/actionTypes";
import apiExecutor from "../../apis/apiExecutor";

export const getAuthorizationToken = state => {
  if (state.userReducerKey.users != null) {
    return state.userReducerKey.authorizationToken;
  } else {
    return null;
  }
};
function* execute(action) {
  let { apiCommand, params } = action.payload;

  console.log("----- start ----- " + action.payload);

  let request = apiExecutor(apiCommand, params);
  if (request == null) {
    yield put({
      type: actionTypes.API_REQUEST_ERROR,
      payload: {
        error: actionTypes.API_REQUEST_ERROR,
      },
    });
    return;
  }
  let { url, args, types } = request;

  console.log("post url " + url);

  let authToken = yield select(getAuthorizationToken);
  const response = yield fetch(url, {
    method: args.method,
    mode: "cors",
    headers: {
      Accept: "application/json",
      Authorization: authToken,
    },

    body: args.method === "POST" ? args.body : null,
  });
  const json = yield response.json();
  // const userId = json.userId;

  const statusCode = response.status;
  if (statusCode === 200) {
    yield put({
      type: types.successActionType,
      payload: {
        headers: response.headers,
        body: json,
      },
    });
  } else if (statusCode === 401) {
    console.log("saga", this.props.location);
    yield put({ type: actionTypes.USER_AUTHENTICATION_FAILURE });
  } else {
    yield put({
      type: types.failureActionType,
      payload: {
        headers: response.headers,
        body: json,
      },
    });
  }
}

function* apiRequestActionWatcher() {
  yield takeLatest(actionTypes.STANDARD_API_CALL, execute);
}
export default function* apiRequestSaga() {
  yield all([apiRequestActionWatcher()]);
}
