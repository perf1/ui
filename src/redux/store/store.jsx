//**********************************************
// Create the global store to hold data
// Assign all the reducers through combinedReducers
//
//**********************************************

import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import combinedReducers from "../reducers/combinedReducers";
import { logger } from "redux-logger";

export const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const loadState = () => {
  try {
    const serializedState = localStorage.getItem("state");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};
const saveState = state => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("state", serializedState);
  } catch (e) {
    // Ignore write errors;
  }
};

const peristedState = loadState();
const store = createStore(
  combinedReducers,
  peristedState,
  composeEnhancers(applyMiddleware(sagaMiddleware, logger))
);

store.subscribe(() => {
  saveState(store.getState());
});

export default store;
