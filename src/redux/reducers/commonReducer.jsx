import * as actionTypes from "../constants/actionTypes";

// import PropTypes from "prop-types";
const initState = {
  error: "",
  loading: false,
};

export default function commonReducer(state = initState, action) {
  let payload = action.payload;

  switch (action.type) {
    case actionTypes.API_REQUEST_ERROR:
      return {
        ...state,

        error: payload.error,
        loading: false,
      };

    default: {
      return state;
    }
  }
}
