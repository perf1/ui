import * as actionTypes from "../constants/menuActionTypes";

// import PropTypes from "prop-types";
const initState = {
  menuOrders: [],
  loading: false,
};

const getBody = payload => {
  return payload.body;
};

export default function menuReducer(state = initState, action) {
  let payload = action.payload;

  switch (action.type) {
    case actionTypes.ADD_ORDER_ITEM:
      console.log("menuReducer", payload);
      const id = payload.id;
      const qty = payload.qty;
      return {
        ...state,
        menuOrders: {
          ...state.menuOrders,
          [id]: {
            id,
            qty,
          },
        },

        loading: false,
      };

    case actionTypes.ADD_ORDER_ITEM_SUCCESS:
      return {
        ...state,
        menuOrders: {
          ...getBody(payload),
        },
        loading: false,
      };
    case actionTypes.ADD_ORDER_ITEM_FAILURE:
      return {
        ...state,
        menuOrders: {
          ...getBody(payload),
        },
        loading: false,
      };

    default: {
      return state;
    }
  }
}
