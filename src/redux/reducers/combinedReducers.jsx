import { combineReducers } from "redux";
import userReducer from "./userReducer";
import orderReducer from "./orderReducer";
import reservationReducer from "./reservationReducer";
import commonReducer from "./commonReducer";
import reviewReducer from "./reviewReducers";

// The key "userReducerKey" will be the key in store to represent this reducer.
export default combineReducers({
  userReducerKey: userReducer,
  orderReducerKey: orderReducer,
  reservationReducerKey: reservationReducer,
  common: commonReducer,
  reviewReducerKey: reviewReducer
});
