import * as actionTypes from "../constants/actionTypes";

const initState = {
  orderItems: [],
  orderSummary: [],
  orderId: "",
  loading: false
};

const getBody = payload => {
  return payload.body;
};

export default function orderReducer(state = initState, action) {
  //let body = null;
  let payload = action.payload;
  switch (action.type) {
    case actionTypes.GET_ORDERITEMS:
      return {
        ...state,
        loading: true
      };
    case actionTypes.GET_ORDERITEMS + "_SUCCESS":
      return {
        ...state,
        orderItems: payload
      };
    case actionTypes.GET_ORDERSUMMARY:
      return {
        ...state,
        loading: true
      };
    case actionTypes.GET_ORDERSUMMARY + "_SUCCESS":
      return {
        ...state,
        orderSummary: {
          ...getBody(payload)
        }
      };
    case actionTypes.GET_ORDERBYUSERID:
      return {
        ...state,
        loading: true
      };
    case actionTypes.GET_ORDERBYUSERID + "_SUCCESS":
      return {
        ...state,
        orderSummary: {
          ...getBody(payload)
        }
      };
    default: {
      return state;
    }
  }
}
