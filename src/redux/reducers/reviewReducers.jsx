import * as actionTypes from "../constants/actionTypes";

// import PropTypes from "prop-types";
const initState = {
  reviews: [],
  newReviewId: "-1",
  loading: false
};

export default function reviewReducer(state = initState, action) {
  let payload = action.payload;

  switch (action.type) {
    case actionTypes.WRITE_REVIEW_SUCCESS:
      return {
        ...state,
        loading: false,
        newReviewId: payload.body.reviewId
      };

    case actionTypes.FIND_REVIEW_BY_MENUITEM_ID_SUCCESS:
      return {
        ...state,
        reviews: payload.body
      };

    default: {
      return {
        ...state,
        loading: false
      };
    }
  }
}
