import * as actionTypes from "../constants/actionTypes";

// import PropTypes from "prop-types";
const initState = {
  users: [],
  loading: false,
};

const getBody = payload => {
  return payload.body;
};
const getAuthToken = payload => {
  let headers = payload.headers;
  let authToken = headers.get("Authorization");
  return authToken;
};
export default function userReducer(state = initState, action) {
  let payload = action.payload;

  switch (action.type) {
    case actionTypes.LOGON_SUCCESS:
      return {
        ...state,
        authorizationToken: getAuthToken(payload),
        authenticated: true,
        users: {
          ...getBody(payload),
        },
        loading: false,
      };
    case actionTypes.LOGON_FAILURE:
      return {
        ...state,
        users: {
          authenticated: false,
          ...getBody(payload),
        },
        loading: false,
      };
    case actionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        authorizationToken: getAuthToken(payload),
        authenticated: true,
        users: {
          ...getBody(payload),
        },
        loading: false,
      };
    case actionTypes.REGISTER_FAILURE:
      return {
        ...state,
        users: {
          authenticated: false,
          ...getBody(payload),
        },
        loading: false,
      };
    case actionTypes.FIND_USER_SUCCESS:
      console.log("userReducer FIND_USER_SUCCESS");
      return {
        ...state,
        users: {
          ...action.payload.body.users,
          address: action.payload.body.address,
        },
        loading: false,
      };
    // case actionTypes.UPDATEPROFILE_SUCCESS:
    //   console.log("userReducer UPDATEPROFILE_SUCCESS", action.payload.body);
    //   return {
    //     ...state,
    //     users: {
    //       ...state.users,
    //       ...getBody(payload),
    //     },
    //     loading: false,
    //   };
    case actionTypes.FIND_USER_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.LOGOFF:
      return {
        ...state,
        authorizationToken: null,
        authenticated: false,
        users: null,
        loading: false,
      };
    default: {
      return state;
    }
  }
}
