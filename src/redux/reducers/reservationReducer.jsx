import * as actionTypes from "../constants/actionTypes";

// import PropTypes from "prop-types";
const initState = {
  reservation: [],
  loading: false,
  result: "",
};

const getBody = payload => {
  return payload.body;
};
//const alert = useAlert();
export default function reservationReducer(state = initState, action) {
  // let body = null;
  // let headers = null;
  // let authToken = null;
  // if (action.payload.body !== "undefined") body = action.payload["body"];
  // if (action.payload.headers) {
  //   headers = action.payload.headers;
  //   authToken = headers.get("Authorization");
  // }
  let payload = action.payload;

  switch (action.type) {
    case actionTypes.RESERVATION_SUCCESS:
      console.log("reservationReducer", "RESERVATION_SUCCESS");
      //this.setState({ result: "success" });
      console.log(state);
      return {
        ...state,
        reservation: {
          ...getBody(payload),
        },
        loading: false,
        result: "success",
      };
    case actionTypes.RESERVATION_FAILURE:
      return {
        ...state,
        reservation: {
          ...getBody(payload),
        },
        loading: false,
      };
    default: {
      return state;
    }
  }
}
