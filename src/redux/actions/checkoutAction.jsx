import * as actionType from "../constants/actionTypes";

export function getOrderItems(payload) {
  return {
    type: actionType.GET_ORDERITEMS,
    payload
  };
}

export function getOrderSummary(payload) {
  return {
    type: actionType.GET_ORDERSUMMARY,
    payload
  };
}
