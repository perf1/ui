import * as actionTypes from "../constants/actionTypes";

export function executeAPIRequest(payload) {
  return {
    type: actionTypes.STANDARD_API_CALL,
    payload,
  };
}
