import * as actionTypes from "../constants/actionTypes";

export function logoff(payload) {
  return {
    type: actionTypes.LOGOFF,
  };
}
