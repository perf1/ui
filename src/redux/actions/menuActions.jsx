import * as actionTypes from "../constants/menuActionTypes";

export function AddMenuItem(payload) {
  return {
    type: actionTypes.ADD_ORDER_ITEM,
    payload,
  };
}
