import { Route } from "react-router-dom";
import React, { lazy } from "react";

const Home = lazy(() => import("../components/common/home"));
const About = lazy(() => import("../components/common/about"));
const Contact = lazy(() => import("../components/common/contact"));
const Faq = lazy(() => import("../components/common/faq"));
const chatroom = lazy(() => import("../components/common/chatroom"));
const gameroom = lazy(() => import("../components/common/game"));
const ReviewTest = lazy(() => import("../components/reviews/reviews-test"));

export default function CommonRoutes() {
  return (
    <div className="page">
      <div id="body">
        <Route exact path="/" component={Home} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/faq" component={Faq} />
        <Route exact path="/chatroom" component={chatroom} />
        <Route exact path="/gameroom" component={gameroom} />
        <Route exact path="/reviews-test" component={ReviewTest} />
      </div>
    </div>
  );
}
