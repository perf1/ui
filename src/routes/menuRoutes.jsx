import { Route } from "react-router-dom";
import React, { lazy } from "react";

const food = lazy(() => import("../components/common/food"));
const drink = lazy(() => import("../components/menu/drink"));

export default function MenuRoutes() {
  return (
    <div>
      <Route exact path="/food" component={food} />
      <Route exact path="/drink" component={drink} />
    </div>
  );
}
