import { Route } from "react-router-dom";
import React, { lazy } from "react";

const MyAccount = lazy(() => import("../components/users/myAccount"));
const LogonForm = lazy(() => import("../components/users/logonForm"));
const SignInForm = lazy(() => import("../components/users/signInForm"));
const Address = lazy(() => import("../components/users/address"));
const AddressForm = lazy(() => import("../components/users/addressForm"));
const Logoff = lazy(() => import("../components/users/logoff"));
const Checkout = lazy(() => import("../components/checkout/checkout"));
export default function UserRoutes() {
  return (
    <div className="page">
      <div id="body">
        <Route exact path="/my-account" component={MyAccount} />
        <Route exact path="/logon-form" component={LogonForm} />
        <Route exact path="/signin-form" component={SignInForm} />
        <Route exact path="/address" component={Address} />
        <Route exact path="/address-form" component={AddressForm} />
        <Route exact path="/logoff" component={Logoff} />
        <Route exact path="/checkout" component={Checkout} />
      </div>
    </div>
  );
}
