import { Route } from "react-router-dom";
import React, { lazy } from "react";

const ReserveTable = lazy(() =>
  import("../components/reservation/reserveTable")
);
const Reservation = lazy(() => import("../components/reservation/reservation"));

export default function ReservationRoutes() {
  return (
    <div className="page">
      <div id="body">
        <Route exact path="/reserveTable" component={ReserveTable} />
        <Route exact path="/reservation" component={Reservation} />
      </div>
    </div>
  );
}
