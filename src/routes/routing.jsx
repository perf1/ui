//**********************************************
// Split the routes configurations to multiple files,
//  and each load with lazyload import
// For example,
//      set user related routes in UserRoutes.
//      set common routes in CommonRoutes.
//
//**********************************************

import React, { Component, Suspense } from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";

// Group different routes into categories, and each load with lazyload import
import CommonRoutes from "./commonRoutes";
import UserRoutes from "./userRoutes";
import ReservationRoutes from "./reservationRoutes";
import MenuRoutes from "./menuRoutes";

class Routing extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Suspense fallback={<div>Loading...</div>}>
            <CommonRoutes />
            <UserRoutes />
            <ReservationRoutes />
            <MenuRoutes />
            {/* Append new route groups below  */}
          </Suspense>
        </Switch>
      </Router>
    );
  }
}

export default Routing;
