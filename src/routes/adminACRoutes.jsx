import { Route } from "react-router-dom";
import React, { lazy } from "react";

const AccessControl = lazy(() => import("../components/admin/accessControl"));

export default function AccessControlRoutes() {
  return (
    <div>
      <Route exact path="/admin/access-control" component={AccessControl} />
    </div>
  );
}
