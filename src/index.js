import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Footer from "./footer";

import "bootstrap/dist/css/bootstrap.min.css";
import "./components/stylesheet/stylesheet.scss";

import * as serviceWorker from "./serviceWorker";

//ReactDOM.render(<Header />, document.getElementById("header"));
ReactDOM.render(<App />, document.getElementById("main"));
ReactDOM.render(<Footer />, document.getElementById("footer"));

serviceWorker.unregister();
