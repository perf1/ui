import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ERRORS } from "./redux/constants/messages";
const mapStateToProps = state => {
  let authenticated = false;
  let error = "";
  if (state.userReducerKey.users != null) {
    authenticated = state.userReducerKey.authenticated;
  }
  if (state.userReducerKey.users != null) {
    error = state.userReducerKey.users.ERROR;
  }

  if (state.common.error != null) {
    error = ERRORS[state.common.error];
  }
  return {
    authenticated,
    error
  };
};

class Header extends Component {
  state = {};
  render() {
    if (!this.props.authenticated && this.props.location) {
      return <Redirect to="/logon-form" />;
    } else {
      return (
        <div id="header" className="row">
          {/* {this.props.error ? (
            <Alert color="primary" id="error">
              {this.props.error}
            </Alert>
          ) : null} */}
          <ul className="navigation col-12 col-md-8">
            <li className="menu-active">
              <a href="/home">Home</a>
            </li>

            <li>
              <a href="/logon-form">
                {this.props.authenticated ? "My account" : "Login"}
              </a>
            </li>

            <li>
              <a href="/food">Food</a>
            </li>
            <li>
              <a href="/drink">Drink</a>
            </li>
            <li>
              <a href="/reservation">Reservation</a>
            </li>
            {this.props.authenticated ? (
              <li>
                <a href="/logoff">Logoff</a>
              </li>
            ) : (
              ""
            )}
            <li>
              <a href="/checkout">Checkout</a>
            </li>

            <li>
              <a href="/chatroom">Chatroom</a>
            </li>
            <li>
              <a href="/gameroom">Gameroom</a>
            </li>
            <li>
              <a href="/reviews-test">Reviews</a>
            </li>
          </ul>
          <a id="logo" href="/home">
            <img
              src="images/logo.png"
              width="230"
              height="170"
              alt="Steak House"
              title="Steak House"
            />
          </a>
        </div>
      );
    }
  }
}
const MyHeader = connect(
  mapStateToProps,
  null
)(Header);

export default MyHeader;
