import React, { Component } from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./components/stylesheet/stylesheet.scss";
class Footer extends Component {
  render() {
    return (
      <div id="footer-content row">
        <ul>
          <li className=" col-12 col-md-8">
            <ul>
              <li className=" col-3 col-md-2">
                <a href="/home">Home</a>
              </li>
              <li className="col-3 col-md-2">
                <a href="/about">About</a>
              </li>
              <li className="col-3 col-md-2">
                <a href="/faq">faq</a>
              </li>
              <li className="col-3 col-md-2">
                <a href="/contact">Contact Us</a>
              </li>
            </ul>
          </li>

          <li className="col-12 col-md-4 ">
            <span> &copy; copyright 2019 &copy;. All rights reserved.</span>
          </li>
        </ul>
      </div>
    );
  }
}
export default Footer;
