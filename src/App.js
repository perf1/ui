import React, { Component } from "react";
import { Provider } from "react-redux";

import Routing from "./routes/routing";
import store, { sagaMiddleware } from "./redux/store/store";
//import rootSaga from "./redux/saga/sagas";
import "bootstrap/dist/css/bootstrap.min.css";
import "./components/stylesheet/stylesheet.scss";
import Header from "./header";
//import chat from "./chat-index";

//import rootSaga from "./redux/saga/sagas";
import apiRequestSaga from "./redux/saga/apiRequestSaga";

//sagaMiddleware.run(rootSaga);
const sagas = {
  //rootSaga,
  apiRequestSaga,
};
for (let name in sagas) {
  sagaMiddleware.run(sagas[name]);
}
class App extends Component {
  state = {};
  render() {
    return (
      <Provider store={store}>
        <Header />
        <Routing />
      </Provider>
    );
  }
}

export default App;
