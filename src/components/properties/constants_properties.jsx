export const CHECKOUT_ITEMS_NAME_KEY = "Name";
export const CHECKOUT_ITEMS_PRICE_KEY = "Unit Price";
export const CHECKOUT_ITEMS_QTY_KEY = "Quantity";
export const CHECKOUT_ITEMS_ID_KEY = "Item Id";
export const CHECKOUT_PRODUCT_TOTAL_KEY = "Product Total: ";
export const CHECKOUT_TAX_KEY = "Tax: ";
export const CHECKOUT_ORDER_TOTAL_KEY = "Order Total: ";
