import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import RegistrationForm from "./registrationForm";
import SignInForm from "./signInForm";
import { TabContent, TabPane, Nav, NavItem, NavLink, Row } from "reactstrap";

const mapStateToProps = state => {
  let authenticated = false;
  let error = "";
  if (state.userReducerKey.users != null) {
    authenticated = state.userReducerKey.authenticated;
    error = state.userReducerKey.users.ERROR;
  }
  console.log("authenticated", authenticated);
  console.log("logonform", error);
  return {
    authenticated,
    error,
  };
};
class LogonForm extends Component {
  state = {
    activeTab: "1",
    signIn: true,
  };
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  render() {
    console.log("logonForm props", this.props);

    if (this.props.authenticated) return <Redirect to="/my-account" />;
    return (
      <div className="content d-flex justify-content-center">
        <div className="body col-10">
          <div className="row fullscreen d-flex align-items-center justify-content-start container">
            <div className="row col-12 pt-4">
              <Nav tabs className="row ">
                <NavItem className="m-0 p-0 btn-main">
                  <NavLink
                    className={this.state.activeTab === "1" ? "active" : ""}
                    onClick={() => {
                      this.toggle("1");
                    }}
                  >
                    Sign In
                  </NavLink>
                </NavItem>
                <NavItem className="m-0 p-0 btn-main">
                  <NavLink
                    className={this.state.activeTab === "2" ? "active" : ""}
                    onClick={() => {
                      this.toggle("2");
                    }}
                  >
                    Registration
                  </NavLink>
                </NavItem>
              </Nav>
            </div>
            <TabContent activeTab={this.state.activeTab} className="row col-12">
              <TabPane tabId="1" className="row col-12">
                <Row>
                  <SignInForm />
                </Row>
              </TabPane>
              <TabPane tabId="2" className="row col-12">
                <Row>
                  <RegistrationForm />
                </Row>
              </TabPane>
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

const LogonFormConnect = connect(mapStateToProps)(LogonForm);
export default LogonFormConnect;
