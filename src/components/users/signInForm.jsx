import React, { Component } from "react";
import { connect } from "react-redux";
import { executeAPIRequest } from "../../redux/actions/genericActions";

const mapDispatchToProps = dispatch => {
  return {
    // logon: payload => dispatch(logon(payload)),
    // register: payload => dispatch(register(payload)),
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload)),
  };
};

class SignInForm extends Component {
  state = {};
  handleLogonFormSubmit = event => {
    event.preventDefault();
    let logonId = event.target.logonId.value;
    if (logonId === null || logonId.length === 0) return;

    let password = event.target.password.value;
    if (password === null || password.length === 0) return;

    let params = {
      logonId: logonId,
      password: password,
    };

    let request = {
      apiCommand: "Logon",
      params,
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  };
  render() {
    return (
      <form
        onSubmit={this.handleLogonFormSubmit}
        className="registrationForm form-group col-12"
      >
        <div className="row">
          <div className="col-12 col-md-4">
            <label>User name</label>
          </div>
          <div className="col-12 col-md-8">
            <input
              name="logonId"
              type="text"
              id="logonId"
              className="form-control"
            />
          </div>
        </div>
        <div className="row mt-1">
          <div className="col-12 col-md-4">
            <label>Password</label>
          </div>
          <div className="col-12 col-md-8">
            <input
              className="form-control"
              name="password"
              id="password"
              type="password"
            />
          </div>
        </div>

        <div className="row">
          <button className=" form-control btn btn-success mt-4">
            Sign in
          </button>
        </div>
      </form>
    );
  }
}

const SignInFormConnect = connect(
  null,
  mapDispatchToProps
)(SignInForm);
export default SignInFormConnect;
