import React, { Component } from "react";
import { connect } from "react-redux";
import { executeAPIRequest } from "../../redux/actions/genericActions";

const mapDispatchToProps = dispatch => {
  return {
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload)),
  };
};

class RegistrationForm extends Component {
  state = {};
  handleRegistrationFormSubmit = event => {
    event.preventDefault();
    let [
      logonId,
      password,
      password2,
      firstName,
      lastName,
      email,
    ] = event.target;
    logonId = logonId.value;
    password = password.value;
    password2 = password2.value;
    firstName = firstName.value;
    lastName = lastName.value;
    email = email.value;

    if (password === null || password.length === 0 || password !== password2) {
      alert("Passwords are not match.");
      return false;
    }

    if (logonId === null || logonId.length === 0) return false;

    let params = {
      logonId,
      password,
      firstName,
      lastName,
      email,
    };

    let request = {
      apiCommand: "register",
      params,
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  };
  render() {
    return (
      <form
        action=""
        className="registrationForm form-group col-md-12"
        onSubmit={this.handleRegistrationFormSubmit}
      >
        <div className="row">
          <div className="col-md-4">
            <label htmlFor="logonId">Logon ID</label>
          </div>
          <div className="col-md-8">
            <input type="text" className="form-control" name="logonId" />
          </div>
        </div>
        <div className="row pt-1">
          <div className="col-md-4">
            <label htmlFor="password">Password</label>
          </div>
          <div className="col-md-8">
            <input type="password" className="form-control" name="password" />
          </div>
        </div>
        <div className="row pt-1">
          <div className="col-md-4 col-sm-12 col-12">
            <label htmlFor="password2">Password</label>
          </div>
          <div className="col-md-8 col-sm-12 col-12">
            <input type="password" className="form-control" name="password2" />
          </div>
        </div>
        <div className="row pt-1">
          <div className="col-md-4">
            <label htmlFor="firstName">First Name</label>
          </div>
          <div className="col-md-8">
            <input type="text" className="form-control" name="firstName" />
          </div>
        </div>
        <div className="row pt-1">
          <div className="col-md-4">
            <label htmlFor="lastName">Last Name</label>
          </div>
          <div className="col-md-8">
            <input type="text" className="form-control" name="lastName" />
          </div>
        </div>
        <div className="row pt-1">
          <div className="col-md-4">
            <label htmlFor="email">Email</label>
          </div>
          <div className="col-md-8">
            {" "}
            <input type="text" className="form-control" name="email" />
          </div>
        </div>
        <div className="row mt-4">
          <button className="btn-success form-control btn">Register</button>
        </div>
      </form>
    );
  }
}

const RegistrationFormConnect = connect(
  null,
  mapDispatchToProps
)(RegistrationForm);
export default RegistrationFormConnect;
