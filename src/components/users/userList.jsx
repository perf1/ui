import React, { Component } from "react";
import { connect } from "react-redux";
import { addUser, deleteUser } from "../../redux/actions/userActions";

const mapStateToProps = state => {
  // console.log("mapStateToProps", state);
  return {
    //userReducerKey is defined through combinedRecers, as the key to this reducer
    users: state.userReducerKey.users,
  };
};

// Map the dispatch to this component's props, therefore, you can define your own function name that will trigger the dispatcher.
const mapDispatchToProps = dispatch => {
  console.log("mapDispatchToProps", dispatch);
  return {
    myAddUser: payload => dispatch(addUser(payload)),
    myRemoveUser: payload => dispatch(deleteUser(payload)),
  };
};

class UserList extends Component {
  // This should be the payload format that saved in store
  state = {
    id: 0,
    userName: "",
  };

  handleAdd = event => {
    event.preventDefault();

    let userName = this.state.userName;
    if (userName === null || userName.length === 0) return;
    let newId = this.state.id + 1;

    console.log("handleAdd", userName);
    // Call the action defined locally, that will trigger the action dispatch
    this.props.myAddUser({
      id: newId,
      userName,
    });

    this.setState({
      id: newId,
      userName: "",
    });
  };

  handleChange = event => {
    event.preventDefault();
    const userName = event.target.value;
    console.log("handleChange", userName);
    this.setState({
      userName,
    });
  };

  handleDelete = (event, userId) => {
    this.props.myRemoveUser({ id: userId });
  };

  render() {
    return (
      <React.Fragment>
        <form onSubmit={this.handleAdd}>
          <div className="row m-5 h2">Add user</div>
          <div className="col-md-6 form-group m-5">
            <label for="add-user">User name</label>
            <input
              className="form-control"
              id="add-user"
              name="userName"
              value={this.state.userName}
              onChange={this.handleChange}
              type="input"
            />

            <button
              name="submit"
              type="submit"
              className="btn btn-primary mt-2"
            >
              Add User
            </button>
          </div>
        </form>

        <div className="row m-5 h2">UserList</div>
        <ul className="col-md-6 m-5">
          {this.props.users.map(user => (
            <li key={user.id}>
              <span className="h5">{user.userName}</span>
              <button
                className="btn btn-secondary ml-4 mb-2"
                onClick={event => {
                  this.handleDelete(event, user.id);
                }}
              >
                x
              </button>{" "}
            </li>
          ))}
        </ul>
      </React.Fragment>
    );
  }
}

const connectList = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);

export default connectList;
