import React, { Component } from "react";
import { connect } from "react-redux";
import Prototypes from "prop-types";
import { executeAPIRequest } from "../../redux/actions/genericActions";

function mapStateToProps(state) {
  let userInfo = "";
  if (state.userReducerKey.users != null) {
    userInfo = state.userReducerKey.users;
  }

  return {
    userId: userInfo.userId,
    userInfo,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload)),
  };
}

class Account extends Component {
  state = {
    isEdit: false,
    userId: this.props.userInfo ? this.props.userInfo.userId : "",
    logonId: this.props.userInfo ? this.props.userInfo.logonId : "",
    address: this.props.userInfo ? this.props.userInfo.address : "",
  };

  handleChangeOnForm = e => {
    e.preventDefault();
    let { name, value } = e.target;
    this.setState({
      ...this.state,
      address: {
        ...this.state.address,
        [name]: value,
      },
    });
  };

  handleEditProfile = e => {
    e.preventDefault();
    this.setState({ isEdit: !this.state.isEdit });
  };

  handleUpdateProfile = e => {
    console.log("myaccount ", "handleUpdateProfile", e.target);
    e.preventDefault();
    console.log("myAccount", "handleUpdateProfile", e.target);

    let logonId = e.target.logonId.value;
    let password = e.target.password.value;
    let password2 = e.target.password2.value;
    let firstName = e.target.firstName.value;
    let lastName = e.target.lastName.value;
    let email = e.target.email.value;

    if (password !== password2) {
      alert("Passwords are not match.");
      return false;
    }

    let params = {
      userId: this.props.userId,
      logonId,
      password,
      firstName,
      lastName,
      email,
    };

    let request = {
      apiCommand: "updateProfile",
      params,
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);

    request = {
      apiCommand: "findUser",
      params: { userId: this.props.userId },
    };

    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);

    this.setState({ ...this.state, isEdit: false });
  };
  componentDidMount() {
    const { userId } = this.props.userInfo;
    console.log("myaccount onload", userId);
    // if (userId != null && userId !== "" && userInfo == null) {
    let request = {
      apiCommand: "findUser",
      params: { userId },
    };

    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  }

  static getDerivedStateFromProps(props, state) {
    if (state.isEdit) return state;
    return {
      userId: props.userInfo ? props.userInfo.userId : "",
      logonId: props.userInfo ? props.userInfo.logonId : "",
      address: props.userInfo ? props.userInfo.address : "",
    };
  }

  render() {
    const { isEdit } = this.state;
    console.log("myAccount", this.props.userInfo);
    const { logonId, address, userId } = this.state;

    if (userId) {
      return (
        <div className="content">
          <div className="body">
            <div className="row">
              <form
                action=""
                className="myaccountForm form-group col-md-12"
                onSubmit={this.handleUpdateProfile}
              >
                <div className="row">
                  <div className="col-md-4">
                    <label htmlFor="logonId">Logon ID</label>
                  </div>
                  <div className="col-md-8">
                    <input
                      type="text"
                      className="form-control"
                      name="logonId"
                      value={logonId}
                      disabled
                    />
                  </div>
                </div>
                <div className="row pt-1">
                  <div className="col-md-4">
                    <label htmlFor="password">Password</label>
                  </div>
                  <div className="col-md-8">
                    <input
                      type="password"
                      className="form-control"
                      name="password"
                      disabled={!isEdit}
                    />
                  </div>
                </div>
                <div className="row pt-1">
                  <div className="col-md-4 col-sm-12 col-12">
                    <label htmlFor="password2">Password</label>
                  </div>
                  <div className="col-md-8 col-sm-12 col-12">
                    <input
                      type="password"
                      className="form-control"
                      name="password2"
                      disabled={!isEdit}
                    />
                  </div>
                </div>

                {address ? (
                  <div key={address.addressId}>
                    <div className="row pt-1">
                      <div className="col-md-4">
                        <label htmlFor="firstName">First Name</label>
                      </div>
                      <div className="col-md-8">
                        <input
                          type="text"
                          className="form-control"
                          name="firstName"
                          value={address.firstName}
                          disabled={!isEdit}
                          onChange={this.handleChangeOnForm}
                        />
                      </div>
                    </div>
                    <div className="row pt-1">
                      <div className="col-md-4">
                        <label htmlFor="lastName">Last Name</label>
                      </div>
                      <div className="col-md-8">
                        <input
                          type="text"
                          className="form-control"
                          name="lastName"
                          value={address.lastName}
                          disabled={!isEdit}
                          onChange={this.handleChangeOnForm}
                        />
                      </div>
                    </div>
                    <div className="row pt-1">
                      <div className="col-md-4">
                        <label htmlFor="email">Email</label>
                      </div>
                      <div className="col-md-8">
                        <input
                          type="text"
                          className="form-control"
                          name="email"
                          value={address.email}
                          disabled={!isEdit}
                          onChange={this.handleChangeOnForm}
                        />
                      </div>
                    </div>
                  </div>
                ) : null}

                <div className="mt-4">
                  {isEdit ? (
                    <div className="row  d-flex justify-content-center">
                      <button className="btn-success form-control btn col-md-4 col-12 m-3">
                        Update
                      </button>
                      <button
                        className="btn-secondary form-control btn col-md-4 col-12 m-3"
                        onClick={this.handleEditProfile}
                      >
                        Cancel
                      </button>
                    </div>
                  ) : (
                    <button
                      className="btn-success form-control btn"
                      onClick={this.handleEditProfile}
                    >
                      {" "}
                      Edit{" "}
                    </button>
                  )}
                </div>
              </form>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

const MyAccount = connect(
  mapStateToProps,
  mapDispatchToProps
)(Account);

mapDispatchToProps.propTypes = {
  actions: Prototypes.object.required,
};
export default MyAccount;
