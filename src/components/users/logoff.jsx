import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { logoff } from "../../redux/actions/userActions";
const mapDispatchToProps = dispatch => {
  return {
    logoff: payload => dispatch(logoff(payload)),
  };
};

class Logoff extends Component {
  render() {
    this.props.logoff();
    return <Redirect to="/logon-form" />;
  }
}
const LogoffConnect = connect(
  null,
  mapDispatchToProps
)(Logoff);
export default LogoffConnect;
