import React, { Component } from "react";
import { connect } from "react-redux";
import { executeAPIRequest } from "../../redux/actions/genericActions";
import * as actionTypes from "../../redux/constants/actionTypes";
import ReviewDisplay from "./review";

const mapStateToProps = state => {
  let userId = "";
  let authenticated = "";
  let reviews = [];
  let newReviewId = "-1";

  if (state.userReducerKey.users != null) {
    userId = state.userReducerKey.users.userId;
    authenticated = state.userReducerKey.authenticated;
  }

  if (state.reviewReducerKey != null) {
    reviews = state.reviewReducerKey.reviews;
    newReviewId = state.reviewReducerKey.newReviewId;
  }

  return {
    userId,
    authenticated,
    reviews,
    newReviewId
  };
};

// Map the dispatch to this component's props, therefore, you can define your own function name that will trigger the dispatcher.
const mapDispatchToProps = dispatch => {
  return {
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload))
  };
};

class Review extends Component {
  state = {
    authenticated: this.props.authenticated,
    reviews: this.props.reviews,
    newReviewId: this.props.newReviewId
  };

  writeReview = event => {
    event.preventDefault();
    let review = this.refs.reviewMessage.value;
    if (review === null || review.length === 0) return;

    let menuItemId = 11;

    let userId = this.props.userId;

    let params = {
      message: review,
      menuItemId: menuItemId,
      userId: userId
    };

    let request = {
      apiCommand: actionTypes.WRITE_REVIEW,
      params
    };

    document.getElementById("reviewMessage").value = "";

    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  };

  findReviews() {
    let menuItemId = 11;

    let params = {
      menuItemId: menuItemId
    };

    let request = {
      apiCommand: actionTypes.FIND_REVIEW_BY_MENUITEM_ID,
      params
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  }

  componentDidMount() {
    console.log("lemon" + JSON.stringify(this.props.userInfo));

    this.findReviews();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.newReviewId !== this.props.newReviewId) {
      this.findReviews();
    }
  }

  reviewSection() {
    console.log(this.props.authenticated + "PINEAPPLE");
    if (this.props.authenticated) {
      return (
        <div className="write-review-container">
          <label forhtml="reviewMessage">Please enter your review below</label>
          <textarea
            type="text"
            ref="reviewMessage"
            id="reviewMessage"
            name="reviewMessage"
          />
          <button onClick={this.writeReview} className="btn btn-primary">
            Submit Review
          </button>
        </div>
      );
    } else {
      return (
        <div className="write-review-container">
          <label>Please sign in to leave a review</label>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="content">
        <div className="reviews-container  body">
          <h2>Reviews</h2>
          {this.props.reviews.map((review, key) => (
            <ReviewDisplay review={review} key={review.reviewId} />
          ))}
          {this.reviewSection()}
        </div>
      </div>
    );
  }
}

const connectList = connect(
  mapStateToProps,
  mapDispatchToProps
)(Review);

export default connectList;
