import React, { Component } from "react";

class ReviewDisplay extends Component {
  render() {
    let dateLong = this.props.review.displayDate;
    let date = new Date(dateLong);

    var theyear = date.getFullYear();
    var themonth = date.getMonth() + 1;
    var thetoday = date.getDate();

    let displayDate = theyear + "-" + themonth + "-" + thetoday;

    return (
      <div className="review">
        <p className="review-name">{this.props.review.username}</p>
        <p className="review-message">"{this.props.review.message}"</p>
        <p className="review-timestamp">{displayDate}</p>
      </div>
    );
  }
}

export default ReviewDisplay;
