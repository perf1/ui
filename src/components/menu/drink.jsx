import React, { Component } from "react";
import { connect } from "react-redux";
import Menus from "./menus";
const mapStateToProps = state => {
  if (state.userReducerKey.users != null) {
    return {
      authToken: state.userReducerKey.authorizationToken,
    };
  }
};
class Drink extends Component {
  state = { data: [] };

  componentDidMount() {
    console.log("drink", "componentdidMount");
    let url = "http://steakhouse.com/api/category/find/name/detail/drink";

    fetch(url, {
      method: "get",
      headers: new Headers({
        Authorization: this.props.authToken,
        "Content-Type": "application/x-www-form-urlencoded",
      }),
    })
      .then(res => res.json())
      .then(json => this.setState({ data: [json] }))
      .catch(error => console.log(error));
  }
  render() {
    let isAdmin = false;
    return <Menus data={this.state.data} isAdmin={isAdmin} />;
  }
}
const DrinkConnect = connect(
  mapStateToProps,
  null
)(Drink);
export default DrinkConnect;
