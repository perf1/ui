import React, { Component } from "react";
import { connect } from "react-redux";

const mapStateToProps = state => {
  if (state.userReducerKey.users != null) {
    return {
      authToken: state.userReducerKey.users.authorizationToken,
    };
  }
};

// const mapDispatchToProps = dispatch => {};

class Menus extends Component {
  state = { data: [] };

  render() {
    return (
      <div id="background">
        <div className="page">
          <div id="body">
            <div className="content">
              <div className="body ">
                <h3>MENU</h3>
                <ul className="section">
                  {this.props.data.map(category => (
                    <li className="row" key={category.categoryId}>
                      <h2 className="col-12">{category.name}</h2>
                      <div className="col-12 text-center">
                        <img src={`images/${category.name}.jpg`} />
                      </div>
                      <ul className="col-12 px-3">
                        {category.menuItems.map(item => (
                          <div className="row">
                            <li key={item.menuItemId} className="col-12 px-5">
                              <div className="row ">
                                <h2 className="col-md-10 col-12">
                                  <a
                                    href="food.html"
                                    className="font-weight-bold"
                                  >
                                    {item.menuItemName}
                                  </a>
                                </h2>
                                <div className="font-weight-bold col-md-2 col-12">
                                  $00.00
                                </div>
                              </div>

                              <div className="col-12">{item.menuItemDesc}</div>
                              <div className="mt-4">
                                {item.attributes.map(attr => (
                                  <div
                                    className="row "
                                    key={
                                      "attr" +
                                      item.menuItemid +
                                      attr.attributeId +
                                      attr.attributeValueId
                                    }
                                  >
                                    <span className="col-md-4 col-12 text-left">
                                      {attr.description}
                                    </span>
                                    <span className="float-right col-md-8 col-12">
                                      {attr.value}
                                    </span>
                                  </div>
                                ))}
                              </div>
                            </li>
                          </div>
                        ))}
                      </ul>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const MenuConnect = connect(
  mapStateToProps,
  null
)(Menus);
export default MenuConnect;
