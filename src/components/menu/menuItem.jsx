import React, { Component } from "react";
import { connect } from "react-redux";
import { executeAPIRequest } from "../../redux/actions/genericActions";
import { AddMenuItem } from "../../redux/actions/menuActions";

const mapStateToProps = state => {
  return { ...state };
};

const mapDispatchToProps = dispatch => {
  return {
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload)),
    addItem: payload => dispatch(AddMenuItem(payload)),
  };
};

class MenuItem extends Component {
  state = {};

  handleAddItem = event => {
    let id = event.target.attributes["data-itemid"].value;
    let item = this.state[id];
    let qty = 0;
    if (item != null) {
      qty = this.state[id].qty;
    }

    this.setState({ [id]: { id, qty: ++qty } });

    // const key = "menuItem-" + id;
    let params = {
      // [key]: {
      id: id,
      qty: qty,
      // },
    };

    // if (qty === 1)
    this.props.addItem(params);
    // else this.props.updateItem(params);

    let request = {
      apiCommand: "AddMenuItem",
      params,
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  };
  handleReduceItem = event => {
    let id = event.target.attributes["data-itemid"].value;

    let item = this.state[id];
    if (item == null) {
      alert("not added yet");
      return;
    }

    let qty = this.state[id].qty;
    if (qty === 0) {
      alert("No more");
      return;
    }

    this.setState({ [id]: { id, qty: --qty } });

    let params = {
      // [key]: {
      id: id,
      qty: qty,
      // },
    };

    // if (qty === 1)
    this.props.addItem(params);
    // else this.props.updateItem(params);

    let request = {
      apiCommand: "AddMenuItem",
      params,
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
  };
  render() {
    const product = this.props.product;
    console.log(product.menuItemId);
    let random = Math.floor(Math.random() * Math.floor(100));
    return (
      <div
        id="products"
        className="row shadow-sm  py-2 mb-5 bg-white rounded mx-3"
      >
        <div className="col-md-3">
          <img src={`https://picsum.photos/id/${random}/140/100`} alt="item" />
        </div>
        <div className=" pl-3 col-md-7">
          <div className="rounded h6">{product.menuItemName}</div>
          <div className="font-weight-light">{product.menuItemDesc}</div>
        </div>
        <input type="hidden" name="id" value={product.menuItemId} />
        <div className="col-md-2 ">
          <div className="float-right mt-3">$12.99</div>
          <div>
            <button
              className="btn btn-secondary   "
              onClick={this.handleReduceItem}
              data-itemid={product.menuItemId}
            >
              -
            </button>
            <button
              className="btn btn-success  "
              onClick={this.handleAddItem}
              data-itemid={product.menuItemId}
            >
              +
            </button>
          </div>
        </div>
      </div>
    );
  }
}
const MenuItemConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuItem);
export default MenuItemConnect;
