import React, { Component } from "react";
import { connect } from "react-redux";
const mapStateToProps = (state = []) => {
  return {
    menuOrders: state.menuReducerKey.menuOrders,
  };
};
class Order extends Component {
  state = {};
  render() {
    let orders = this.props.menuOrders;
    const keys = Object.keys(orders);

    return (
      <div>
        <div className="h5">Orders</div>
        {keys.map(key => {
          return <div product={key.name} id={key.menuItemId} />;
        })}
      </div>
    );
  }
}

const OrderConnect = connect(
  mapStateToProps,
  null
)(Order);
export default OrderConnect;
