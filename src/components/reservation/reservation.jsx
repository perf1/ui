import React, { Component } from "react";

import { connect } from "react-redux";
import { executeAPIRequest } from "../../redux/actions/genericActions";

const mapDispatchToProps = dispatch => {
  return {
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload))
  };
};

function mapStateToProps(state) {
  let result = "";

  if (state.reservationReducerKey.result != null) {
    result = state.reservationReducerKey.result;
  }

  return {
    result
  };
}
class Reservation extends Component {
  state = { maxSeats: 12, result: "" };

  handleReservationFormSubmit = event => {
    event.preventDefault();
    let [name, seats, contact, specialRequest, time] = event.target;
    name = name.value;
    seats = seats.value;
    contact = contact.value;
    specialRequest = specialRequest.value;
    time = time.value;

    if (name === null || name.length === 0) return false;
    if (contact === null || contact.length === 0) return false;
    //if (specialRequest === null || specialRequest.length === 0) return false;
    if (time === null || time.length === 0) return false;

    let params = {
      name,
      seats,
      contact,
      time,
      specialRequest
    };

    let request = {
      apiCommand: "createReservation",
      params
    };
    // Call the action defined locally, that will trigger the action dispatch
    this.props.executeAPIRequest(request);
    this.setState({ result: this.props.result });
  };

  generateSeatsDropdown() {
    let select = [];
    for (let i = 1; i <= 12; i++) {
      select.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }
    return select;
  }

  render() {
    //let result = this.state.result;
    const msgDivClass = this.state.result === "success" ? "" : "d-none";
    const formDivClass = this.state.result !== "success" ? "" : "d-none";
    return (
      <div className="content">
        <div className="body">
          <h3 className="mb-10">Make a Reservation</h3>
          <h6>Please input the information and reserve a table online</h6>

          <div className={msgDivClass} id="resultMsg">
            Your Reservation is submitted successfully.
          </div>

          <form
            id="reservationForm"
            className={formDivClass}
            onSubmit={this.handleReservationFormSubmit}
          >
            <div className="row">
              <div className="col-4">
                <label>Name:</label>
              </div>
              <div className="col-8">
                <input name="name" type="text" className="form-control" />
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>How many seats:</label>
              </div>
              <div className="col-8">
                <select className="custom-select" name="seats">
                  {this.generateSeatsDropdown()}
                </select>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>Contact:</label>
              </div>
              <div className="col-8">
                <input name="contact" type="text" className="form-control" />
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>Time(e.g. 2019/01/31 19:00):</label>
              </div>
              <div className="col-8">
                <input name="time" type="text" className="form-control" />
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>Special Request:</label>
              </div>
              <div className="col-8">
                <input
                  name="specialRequest"
                  type="text"
                  className="form-control"
                />
              </div>
            </div>
            <div className="row">
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
const ReservationFormConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Reservation);
export default ReservationFormConnect;
