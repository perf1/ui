import React, { Component } from "react";
import { connect } from "react-redux";

const mapStateToProps = state => {
  if (state.userReducerKey.users != null) {
    return {
      authToken: state.userReducerKey.users.authorizationToken
    };
  }
};

class ReserveTable extends Component {
  state = { data: [] };

  componentDidMount() {
    window.addEventListener("load", this.getTableList());
  }

  getTableList() {
    console.log("get table list:");
    fetch("http://localhost:8080/reserveTable/find-all", {
      method: "get",
      headers: new Headers({ Authorization: this.props.authToken })
    })
      .then(res => {
        //console.log(res.json());
        return res.json();
      })
      .then(json => this.setState({ data: json }))
      .catch(error => console.log(error));
    //$("myclass") //  $ is available here
  }
  render() {
    console.log(this.state.data);
    return (
      <div className="content">
        <div className="body">
          <h1 class="mb-10">Table list</h1>
          <p>Which table you would like to choose</p>

          {this.state.data.map(table => (
            <div className="col-lg-4">
              <div className="single-menu">
                <div className="title-div justify-content-between d-flex">
                  <h4>Table: {table.tableId}</h4>
                  <p class="price float-right">{table.tableType}</p>
                </div>
                <p>This table is available</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
const ReserveTableConnect = connect(
  mapStateToProps,
  null
)(ReserveTable);
export default ReserveTableConnect;
