import React, { Component } from "react";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";
import { findDOMNode } from "react-dom";
import {
  Navbar,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

const mapStateToProps = state => {
  if (state.userReducerKey.users != null) {
    return {
      authToken: state.userReducerKey.users.authorizationToken,
    };
  }
};
class AcRole extends Component {
  handleSelectRole = event => {
    event.preventDefault();

    let value = event.target.text;
    this.setState({ roleName: value });
  };
  componentDidMount() {
    let url = "http://localhost:8080/admin/access-control/roles/find-all";

    fetch(url, {
      method: "get",
      headers: new Headers({
        Authorization: this.props.authToken,
        "Content-Type": "application/x-www-form-urlencoded",
      }),
    })
      .then(res => res.json())
      .then(json => this.setState({ data: json }))
      .catch(error => console.log(error));
  }
  state = { data: [], roleName: "" };
  render() {
    return (
      <div className="role">
        <UncontrolledDropdown setActiveFromChild>
          <DropdownToggle tag="a" className="nav-link" caret>
            {this.state.roleName !== ""
              ? this.state.roleName
              : this.props.roleName}
          </DropdownToggle>
          <DropdownMenu>
            {this.state.data.map(role => (
              <DropdownItem
                tag="a"
                href="#"
                onClick={this.handleSelectRole}
                key={role.roleId}
              >
                {role.name}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>
    );
  }
}

const AcRoleConnect = connect(
  mapStateToProps,
  null
)(AcRole);
export default AcRoleConnect;
