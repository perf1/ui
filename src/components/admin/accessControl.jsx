import React, { Component } from "react";
import { connect } from "react-redux";
import AcMask from "./accessControlMask";
import AcRole from "./acRole";
import UserGroup from "./userGroup";
// const mapStateToProps = state => {
//   console.log("Accesscontrol", state);
//   return {};
// };
// const mapDispatchToProps = dispatch => {
//   return null;
// };
class AccessControl extends Component {
  state = { data: {} };
  render() {
    return (
      <div className="row">
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">PolicyName</th>
              <th scope="col">Role</th>
              <th scope="col">Class Group</th>
              <th scope="col">
                <span className="">R</span>
                <span className="">W</span>
                <span className="">U</span>
                <span className="">D</span>
                <span className="">A</span>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Policy1</td>
              <td>
                <AcRole roleName="Admin" />
              </td>
              <td>
                <UserGroup />
              </td>
              <td>
                <AcMask />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

const AccessControlConnect = connect(
  null,
  null
)(AccessControl);
export default AccessControlConnect;
