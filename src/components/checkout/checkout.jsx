import React, { Component } from "react";
import { connect } from "react-redux";
import { executeAPIRequest } from "../../redux/actions/genericActions";
import OrderItemList from "./orderItemList";
import OrderSummary from "./orderSummary";
import * as Constants from "../properties/constants_properties";

const mapStateToProps = state => {
  let orderItems = "";
  let orderSummary = "";
  let userId = "";
  if (state.userReducerKey.users != null) {
    userId = state.userReducerKey.users.userId;
  }
  if (state.orderReducerKey.orderSummary != null) {
    orderSummary = state.orderReducerKey.orderSummary;
  }
  if (state.orderReducerKey.orderItems != null) {
    //orderId = state.orderReducerKey.orderId;
    orderItems = state.orderReducerKey.orderItems;
  }
  return {
    orderItems,
    orderSummary,
    userId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    executeAPIRequest: payload => dispatch(executeAPIRequest(payload)),
  };
};

class Checkout extends Component {
  state = {};

  componentDidMount() {
    //TODO userID
    let { orderSummary, userId } = this.props;

    //Get Order
    let request = {
      apiCommand: "GetOrderByUserId",
    };
    if (
      userId != null &&
      userId !== "" //&&
      //(orderSummary == null || orderSummary.length === 0)
    ) {
      request.params = { userId };
      this.props.executeAPIRequest(request);
    }

    //Get Items
    request = {
      apiCommand: "GetItemsByOrderId",
    };
    if (
      orderSummary != null &&
      orderSummary !== "" &&
      orderSummary.ordersId != null &&
      orderSummary.ordersId !== "" //&&
      //(orderItems == null || orderItems.length === 0)
    ) {
      let orderId = orderSummary.ordersId;
      request.params = { orderId };
      this.props.executeAPIRequest(request);
    }
    //Get summary
    // request = {
    //   apiCommand: "GetOrderSummaryByOrderId"
    // };
    // if (orderId != null && orderId !== "" && orderSummary == null) {
    //   request.params = { orderId };
    //   this.props.executeAPIRequest(request);
    // }
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-12">Checkout </div>
        </div>

        <div className="row">
          <div className="col-md-8 left">Items</div>
          <div className="col-md-4 right">Summary</div>
        </div>
        <div className="row">
          <div className="row col-md-8 left">
            <div className="row itemsHeader col-md-12 left">
              <span className="col-md-3 left">
                {Constants.CHECKOUT_ITEMS_ID_KEY}
              </span>
              <span className="col-md-3 left">
                {Constants.CHECKOUT_ITEMS_NAME_KEY}
              </span>
              <span className="col-md-3 left">
                {Constants.CHECKOUT_ITEMS_QTY_KEY}
              </span>
              <span className="col-md-3 left">
                {Constants.CHECKOUT_ITEMS_PRICE_KEY}
              </span>
            </div>
            <OrderItemList items={this.props.orderItems.body} />
          </div>
          <div className="col-md-4 right">
            <OrderSummary items={this.props.orderSummary} />
          </div>
        </div>
      </div>
    );
  }
}

const checkout = connect(
  mapStateToProps,
  mapDispatchToProps
)(Checkout);

export default checkout;
