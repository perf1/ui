import React from "react";
function orderItemList(params) {
  if (
    params.items != null &&
    params.items !== undefined &&
    params.items.length > 0
  ) {
    return (
      <div className="row col-md-12 left">
        {params.items.map(orderitem => (
          <div className="row col-md-12" key={orderitem.orderitemId}>
            <span id="id" className="col-md-3 left">
              {orderitem.orderitemId}
            </span>
            <span id="name" className="col-md-3 left ">
              {orderitem.orderitemId}
            </span>
            <span id="qty" className="col-md-3 left ">
              {orderitem.quantity}
            </span>
            <span id="price" className="col-md-3 left ">
              ${orderitem.price}
            </span>
          </div>
        ))}
      </div>
    );
  } else {
    return (
      <div>
        <div className="col-md-8 left">
          <div>Empty Cart</div>
        </div>
      </div>
    );
  }
}

export default orderItemList;
