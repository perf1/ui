import React from "react";
import * as Constants from "../properties/constants_properties";
function orderSummary(params) {
  return (
    <div>
      <div className="row">
        <span>{Constants.CHECKOUT_PRODUCT_TOTAL_KEY}</span>
        <span>{params.items.totalProduct}</span>
      </div>
      <div className="row">
        <span>{Constants.CHECKOUT_TAX_KEY}</span>
        <span>{params.items.totalTax}</span>
      </div>
      <div className="row">
        <span>{Constants.CHECKOUT_ORDER_TOTAL_KEY}</span>
        <span>{params.items.totalProduct + params.items.totalTax}</span>
      </div>
    </div>
  );
}

export default orderSummary;
