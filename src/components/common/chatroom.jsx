import React, { Component } from "react";
import Chat from "./../../chat-index";
class chatroom extends Component {
  render() {
    return (
      <div className="content">
        <div className="body">
          <h3>Online chatroom</h3>
          <Chat />
        </div>
      </div>
    );
  }
}

export default chatroom;
