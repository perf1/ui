import React, { Component } from "react";
class Contact extends Component {
  state = {};
  render() {
    return (
      <div class="content">
        <div class="body">
          <h2>CONTACT US</h2>
          <ul>
            <li>
              <h4>You can replace all this text with your own text</h4>
              <p>
                This website template has been designed by{" "}
                <a href="http://www.freewebsitetemplates.com/">
                  Free Website Templates
                </a>{" "}
                for you, for free. You can replace all this text with your own
                text You can remove any links to our website from this website
                template, you're free to use this website template without
                linking back to us. If you're having a problems editing this
                temlate, then don't hesitate to ask for help on the{" "}
                <a href="http://www.freewebsitetemplates.com/forums">Forums</a>.
              </p>
            </li>
          </ul>
          <div id="contact">
            <label>
              <span>Store Location</span>:32 Morbi Suscipit Semquis Aliquet
              Consequat, 1234
            </label>
            <label>
              <span>Store hours</span>:Monday to Sunday : 10:30am - 9:30pm
            </label>
            <label>
              <span>Reservation Number</span>:(864) 232-3706
            </label>
            <label>
              <span>Office Phone Number</span>:(864) 232-3553
            </label>
            <label>
              <span>Fax Number</span>:(864) 232-4160
            </label>
            <label>
              <span>Email</span>:emailus@companyname.com
            </label>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
