import React, { Component } from "react";
import TicTacToe from "./tic-tac-toe";
import Sudoku from "./sudoku";
class Game extends Component {
  state = {};
  render() {
    return (
      <div className="content">
        <div className="body">
          <h3>Game room</h3>
          <TicTacToe />
          <br />
          <Sudoku />
        </div>
      </div>
    );
  }
}

export default Game;
