import React, { Component } from "react";
class Home extends Component {
  state = {};
  render() {
    console.log("call apiExecutor");
    return (
      <div id="body">
        <div className="slider">
          <ul>
            <li className="first">
              <h2>This is just a place holder</h2>
              <img src="images/nine-dollars.gif" alt="" />
              <p>
                This is just a place holder, so you can see what the site would
                look like.
              </p>
              <a href="/reservation">BOOK A TABLE</a>
            </li>
            <li>
              <a href="/home">
                <img
                  src="images/barbeque.jpg"
                  width="640"
                  height="331"
                  alt="Steak House"
                  title="Steak House"
                />
              </a>
            </li>
          </ul>
        </div>
        <ul id="featured">
          <li className="first">
            <a href="/food">
              <img
                src="images/food-menu.jpg"
                alt="steak house"
                title="steak house"
              />
            </a>
          </li>
          <li>
            <a href="/drink">
              <img
                src="images/beer.jpg"
                alt="steak house"
                title="steak house"
              />
            </a>
          </li>
        </ul>
        <div className="section">
          <h3>$7 LUNCH & DINNER BUFFET </h3>
          <p>
            This is a place holder, so you can see what the site would look
            like.
          </p>
        </div>
        <div id="content">
          <div className="article">
            <ul>
              <li>
                <h2>
                  <a href="about">About the company</a>
                </h2>
                <a href="about">
                  <img
                    src="images/waiter.jpg"
                    width="162"
                    height="101"
                    alt="steak house"
                    title="steak house"
                  />
                </a>
                <p>
                  This is just a place holder, so you can see what the site
                  would look like. You can replace all this text with your own
                  text. You can remove any link to our website from this website
                  template, you're free to use this website template without
                  linking back to us. <a href="about/more">(more)</a>
                </p>
              </li>
              <li>
                <h2>
                  <a href="history">OUR humble beginnings</a>
                </h2>
                <a href="history">
                  <img
                    src="images/food.jpg"
                    width="162"
                    height="122"
                    alt="steak house"
                    title="steak house"
                  />
                </a>
                <p>
                  You can replace all this text with your own text. You can
                  remove any link to our website from this website template,
                  you're free to use this website template without linking back
                  to us. This is just a place holder, so you can see what the
                  site would look like. <a href="history-detail">(more)</a>
                </p>
              </li>
            </ul>
          </div>
          <div className="aside">
            <h3>blog</h3>
            <ul>
              <li>
                <h2>This is just a place holder</h2>
                <p>
                  You can remove any link to our website from this website
                  template.
                </p>
              </li>
              <li>
                <h2>This is just a place holder</h2>
                <p>
                  This is just a place holder, so you can see what the site
                  would look like.
                </p>
              </li>
              <li className="last">
                <h2>This is just a place holder</h2>
                <p>
                  You're free to use this website template without linking back
                  to us.
                </p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
