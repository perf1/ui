import React, { Component } from "react";
import { connect } from "react-redux";
import Menus from "../menu/menus";
const mapStateToProps = state => {
  if (state.userReducerKey.users != null) {
    return {
      authToken: state.userReducerKey.authorizationToken,
      roles: state.userReducerKey.users.roles,
    };
  }
};
class Food extends Component {
  state = { data: [] };

  componentDidMount() {
    console.log("drink", "componentdidMount");
    let url = "http://steakhouse.com/api/category/find/name/detail/dinner";

    fetch(url, {
      method: "get",
      headers: new Headers({
        Authorization: this.props.authToken,
        "Content-Type": "application/x-www-form-urlencoded",
      }),
    })
      .then(res => res.json())
      .then(json => this.setState({ data: json.childCategories }))
      .catch(error => console.log(error));
  }
  render() {
    console.log("state category", this.state.data);
    let isAdmin = false;
    this.props.roles &&
      this.props.roles.map(role => {
        if (role.name === "ROLE_ADMIN") {
          isAdmin = true;
        }
        return isAdmin;
      });
    // console.log("food", "isAdmin", isAdmin);
    return <Menus data={this.state.data} isAdmin={isAdmin} />;
  }
}
const FoodConnect = connect(
  mapStateToProps,
  null
)(Food);
export default FoodConnect;
