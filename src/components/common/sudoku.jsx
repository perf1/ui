import React from "react";
import "./../../styles/game.css";

class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
      index: this.props.index
    };
  }

  render() {
    return this.state.value != null ? (
      <input
        key={this.state}
        className="square readonly"
        value={this.state.value}
        readOnly
      />
    ) : (
      <input
        key={this.state}
        className="square"
        size="1"
        maxLength="1"
        index={this.state.index}
        onChange={this.props.onChange}
      />
    );
  }
}

class Block extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: this.props.input,
      result: this.props.input
    };
  }
  update = element => {
    const val = element.target.value;
    const index = element.target.getAttribute("index");
    const newResult = this.state.result;
    newResult[index] = val;
    this.setState({ result: newResult });
    this.props.onChange();
  };
  renderSquare(i) {
    return (
      <Square
        key={i}
        value={this.props.input[i]}
        index={i}
        onChange={this.update}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="block">
          <div className="board-row">
            {[0, 1, 2].map(x => this.renderSquare(x))}
          </div>
          <div className="board-row">
            {[3, 4, 5].map(x => this.renderSquare(x))}
          </div>
          <div className="board-row">
            {[6, 7, 8].map(x => this.renderSquare(x))}
          </div>
        </div>
      </div>
    );
  }
}

class Board extends React.Component {
  renderBlock(i) {
    return (
      <Block
        key={i}
        input={this.props.setting.block[i]}
        onChange={this.props.onChange}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="row">{[0, 1, 2].map(x => this.renderBlock(x))}</div>
        <div className="row">{[3, 4, 5].map(x => this.renderBlock(x))}</div>
        <div className="row">{[6, 7, 8].map(x => this.renderBlock(x))}</div>
      </div>
    );
  }
}

class Sudoku extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      block: [
        [, , , , 2, 3, , , 1],
        [, 3, , 7, 6, , , , 9],
        [, , 8, , , , 3, 7, ,],
        [9, 1, , 3, 8, , 7, , 4],
        [, , , 9, , 5, , , ,],
        [5, , 4, , 6, 7, , 8, 9],
        [, 6, 9, , , , 4, , ,],
        [8, , , , 9, 6, , 2, ,],
        [7, , , 4, 5, , , , ,]
      ]
    };
  }
  onChange() {
    console.log("game change");
    //validate result here
  }
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <h2>Sudoku</h2>
          <Board key="sudoku" setting={this.state} onChange={this.onChange} />
          <button>Restart</button>
        </div>
      </div>
    );
  }
}

export default Sudoku;
