import * as apiConstants from "../apiEnvironmentSetup";

export const listTables = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/reserveTable/find-all",
  method: "GET"
};

export const addTable = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/reserveTable/add",
  method: "POST"
};

export const deleteTable = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/reserveTable/delete/{id}",
  method: "DELETE"
};

export const createReservation = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/reservationRecord/create",
  method: "POST"
};
