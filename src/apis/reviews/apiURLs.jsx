import * as apiConstants from "../apiEnvironmentSetup";

export const WRITE_REVIEW = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/reviews/create",
  method: "POST"
};

export const FIND_REVIEW_BY_MENUITEM_ID = {
  URL:
    apiConstants.REST_SERVICE_HOST_NAME +
    "/reviews/find/reviewByMenuItemId/{menuItemId}",
  method: "GET"
};
