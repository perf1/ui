// import * as usersURLs from "./users/apiURLs";
// import * as commonURLs from "./common/apiURimport { appendFile } from "fs";

import * as apiConstants from "./apiConstants";

//*********************************************
//* To abstract all external API calls, below are needed:
//* 1.  Define each external API URL and its unique request parameters through
//*     /api/<componentsGroup>/apiURLs.jsx. according its own functional group
//* 2. Import each apiURLs component into this file, append to below apiURLGroups
//* 3. In each component that needs calling external APIs, it needs to call this apiExecutor
//      function,
//      Sample Logon request:
//
//     params  = apiExecutor("users", "Logon", {logonId:id, password:pwd})
//
//     the component needs connect to executeAPIRequest(request) action through its mapDispatchToProps().
//     Then make below request:
//     this.props.executeAPIRequest(params);
//
//
//*********************************************

//Definition:
// The key will be used as apiGroupKey in  apiExecutor function.
// const apiURLGroups = {
//   users: usersURLs,
//   common: commonURLs,
// };

// private: get authorization token from state
export const getAuthorizationToken = state => {
  // console.log("sags getAuthorization", state);
  if (state.userReducerKey.users != null)
    return state.userReducerKey.users.authorizationToken;
};

//*********************************************************************************** */
// This function will be used to locate the target API URL configurations by its key lookup.
// And prepare its API request parameters.
// If no headers are sent through request params, the defaultHeaders will be used.
//
//
// @param apiGroupKey, the key to lookup the apiURLGroup from apiURLGroups
// @param apiURLFunctionName, the target function name to be executed
// @param params, the request parameters for API call
// @return body, this response will be used as action payload for dispatch action
//*********************************************************************************** */
export const apiExecutor = (
  apiURLFunctionName,
  params,
  successActionType,
  failureActionType
) => {
  let apiURLDefinition = "";

  for (let key in apiConstants.apiURLs) {
    let group = apiConstants.apiURLs[key];
    // console.log("url", group[apiURLFunctionName]);
    apiURLDefinition = group[apiURLFunctionName];
    if (apiURLDefinition != null) break;
  }

  if (apiURLDefinition == null) return;
  let { URL, method, headers } = apiURLDefinition;
  URL = setPathVariable(URL, params);
  if (URL == null) {
    return null;
  }
  if (headers == null) {
    headers = apiConstants.defaultHeaders;
  }

  if (successActionType == null) {
    successActionType = apiURLFunctionName.toUpperCase() + "_SUCCESS";
  }

  if (failureActionType == null) {
    failureActionType = apiURLFunctionName.toUpperCase() + "_FAILURE";
  }
  let formData = new FormData();
  if (method === "POST") {
    for (let key in params) {
      let value = params[key];
      formData.append(key, value);
    }
    // console.log(params);
    // params.forEach(param => console.log("formdata", param));
  } else if (method === "GET") {
    URL += "?";
    for (let key in params) {
      let value = params[key];
      URL += key + "=" + value + "&";
    }
    // console.log(params);
    // params.forEach(param => console.log("formdata", param));
  }

  let body = {
    url: URL,
    args: {
      method: method,
      headers: headers,
      body: formData,
    },
    types: {
      successActionType: successActionType,
      failureActionType: failureActionType,
    },
  };

  return body;
};

const setPathVariable = (URL, params) => {
  let pos1 = URL.indexOf("{");
  let pos2 = URL.indexOf("}");
  if (pos1 >= 0 && pos2 > 0) {
    let pathVar = URL.substring(pos1 + 1, pos2);
    let value = params[pathVar];
    let newURL = URL;
    if (value != null) {
      newURL = URL.substring(0, pos1) + value + URL.substring(pos2 + 1);
    } else {
      return null;
    }

    return setPathVariable(newURL, params);
  } else {
    return URL;
  }
};

export default apiExecutor;
