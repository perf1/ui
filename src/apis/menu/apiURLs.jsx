import * as apiConstants from "../apiEnvironmentSetup";

export const AddMenuItem = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/api/order/add",
  method: "POST",
};
