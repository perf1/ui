import * as usersURLs from "./users/apiURLs";
import * as reservationURLs from "./reservation/apiURLs";
import * as commonURLs from "./common/apiURLs";
import * as orderURLs from "./checkout/apiURLs";
import * as reviewsURLs from "./reviews/apiURLs";

export const defaultHeaders = {
  headers: {
    Accept: "application/json",
    Authorization: ""
  }
};

export const corsMode = {
  mode: "cors"
};

//**************************************
// Group all APIURLs into a single constant for lookup
//
//
//*************************************
export const apiURLs = {
  usersURLs,
  commonURLs,
  orderURLs,
  reservationURLs,
  reviewsURLs
};
