import * as apiConstants from "../apiEnvironmentSetup";

export const Logon = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/account/login",
  method: "POST",
};

export const findUser = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/user/find/{userId}",
  method: "GET",
};

export const register = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/account/register",
  method: "POST",
};

export const updateProfile = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/account/update/{userId}",
  method: "POST",
};
