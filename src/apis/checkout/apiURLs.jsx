import * as apiConstants from "../apiEnvironmentSetup";

export const GetItemsByOrderId = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/order/findItemByOrder/{orderId}",
  method: "GET"
};

export const GetOrderSummaryByOrderId = {
  URL: apiConstants.REST_SERVICE_HOST_NAME + "/order/findOrderById/{orderId}",
  method: "GET"
};

export const GetOrderByUserId = {
  URL:
    apiConstants.REST_SERVICE_HOST_NAME + "/order/findOrderByUserId/{userId}",
  method: "GET"
};
